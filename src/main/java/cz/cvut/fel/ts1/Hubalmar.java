package cz.cvut.fel.ts1;

public class Hubalmar {
    public long factorial (int n){
        int fac = 1;
        if (n < 0){
            return -1;
        }
        for (int i = 1; i < n + 1; i++) {
            fac = fac*i;
        }
        return fac;
    }
}

