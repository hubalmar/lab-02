package cz.cvut.fel.ts1;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class HubalmarTest {
    @Test
    public void factorialTest() {
        Hubalmar ahoj = new Hubalmar();
        assertEquals(120,ahoj.factorial(5));
        assertEquals(5040,ahoj.factorial(7));
        assertEquals(5040,ahoj.factorial(7));
    }
}